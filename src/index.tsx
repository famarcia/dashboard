//Packages
import React                from 'react';
import ReactDOM             from 'react-dom';
import * as serviceWorker   from './serviceWorker';
import 						     "./js/polyfill";

//Styles
import "bootstrap/dist/css/bootstrap.css";
import "@aposoftworks/yamiassu/dist/index.css";
import "@fortawesome/fontawesome-free/css/all.css";
import "react-toastify/dist/ReactToastify.css";
import "./css/app.css";

//Components
import System from "./js/components/System";

//Bootstrap
import "./js/interfaces/window";

ReactDOM.render(
	<System csrf={false} />,
  	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
