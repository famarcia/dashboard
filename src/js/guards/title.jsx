export default function title (_arguments, data) {
    const basetitle 		= data.router["basetitle"];
	const subtitle  		= data.route["title"];
	const basetitlestring 	= Array.isArray(basetitle) ? basetitle[0]:basetitle;
	const midtitlestring	= Array.isArray(basetitle) ? basetitle[1]:"-";

    //Update page title
    if (basetitle && subtitle)  document.title = basetitlestring + midtitlestring + subtitle;
    else if (basetitle)         document.title = basetitlestring;
    else                        document.title = subtitle;

    //Update dashview title
    data.router.context.settitle(subtitle);

    //No need of blocking
    return true;
}
