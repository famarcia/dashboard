//Packages
import React from 'react';

//Interface
export interface iApp {
	//States
	title : string,
}

//Dumb data
const data = {
	title : "",
}

//Context
const appcontext = React.createContext<iApp>(data);
export default appcontext;