//Packages
import React from 'react';
import axios from "axios";

//Interfaces
import iUser 				from "../interfaces/user";
import { AxiosInstance } 	from 'axios';

//Interface
export interface iSystem {
	//Methods
	login		(data: Object)	: Promise<any>,
	register	(data: Object)	: Promise<any>,
	logged		()				: Promise<any>,
	logout		()				: void,
	refresh 	()				: void,
	refetch 	()				: Promise<any>,
	setuser 	(data:any)		: void,
	setfakeuser?(data:any)		: void,
	
	//States
	user					: iUser | undefined | boolean,
	request					: AxiosInstance,
	loading					: boolean,
	waiting 				: boolean,
	shouldRefresh			: any,
}

//Dumb data
const data = {
	login 		: (data : Object) 	=> { return Promise.resolve(false) },
	register	: (data : Object) 	=> { return Promise.resolve(false) },
	logged 		: () 				=> { return Promise.resolve(false) },
	logout 		: ()				=> {},
	refresh		: ()				=> {},
	refetch		: () 				=> { return Promise.resolve(false) },
	setuser 	: (data : any)		=> {},

	user			: undefined,
	request 		: axios,
	loading 		: true,
	waiting 		: true,
	shouldRefresh 	: null,
}

//Context
const systemcontext = React.createContext<iSystem>(data);
export default systemcontext;