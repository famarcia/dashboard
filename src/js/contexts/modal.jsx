//Core
import * as React 	from "react";

//-------------------------------------------------
// Modal context
//-------------------------------------------------

//The context handler
export const ModalContext = React.createContext({});

//The context component
export default function ModalContainer ({children}) {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	const [ display, setDisplay ]				= React.useState("");
	const [ fullscreen, setFullscreen ]			= React.useState(true);
	const [ modals, setModals ]					= React.useState({});

	//-------------------------------------------------
	// Callbacks
	//-------------------------------------------------

	//Add modal to be renderable
	const addModal = React.useCallback((modal) => {
		let _modals = modals;
		_modals[modal.name] = modal;
		setModals(_modals);
	}, [modals]);

	//Remove modal from renderable list
	const removeModal = React.useCallback((modal) => {
		let _modals = modals;
		delete _modals[modal];
		setModals(_modals);
	}, [modals]);

	//Show modal
	const show = React.useCallback((name) => {
		setDisplay(name);
	}, []);

	//Hide current displayed modal
	const hide = React.useCallback((event) => {
        if (event) event.preventDefault();
		setDisplay("");
	}, []);

	//Toggle between fullscreen and normal
	const expand = React.useCallback(() => {
		setFullscreen(!fullscreen);
    }, [fullscreen]);

    //Check if a specific modal exists
    const exists = React.useCallback((name) => {
        return !!modals[name];
    }, [modals]);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<ModalContext.Provider value={{addModal, removeModal, show, hide, display, fullscreen, expand, exists}}>
			{children}
		</ModalContext.Provider>
    );
}
