//Packages
import React 			from 'react';
import { Scrollbars }	from "react-custom-scrollbars";

//Styling
import cardstyle 		from "../../../css/modules/card.module.css";
import unloggedstyle	from "../../../css/modules/views/unlogged.module.css";

//Components
import Button from '../../components/buttons';

export default function Help () {

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<div>
			<p className="text-center pb-4">Um canal com perguntas e respostas rápidas para te ajudar esclarecer as dúvidas com o aplicativo.</p>

			<Scrollbars style={{width:"100%",height:"50vh"}} autoHide>
				<div className={cardstyle.card}>
					<h4>Como fazer cadastro?</h4>
					<p>Clique na sessão Cadastro e preencha todos os campos</p>
				</div>

				<div className={cardstyle.card}>
					<h4>Como fazer cadastro?</h4>
					<p>Clique na sessão Cadastro e preencha todos os campos</p>
				</div>
				
				<div className={cardstyle.card}>
					<h4>Como fazer cadastro?</h4>
					<p>Clique na sessão Cadastro e preencha todos os campos</p>
				</div>
				
				<div className={cardstyle.card}>
					<h4>Como fazer cadastro?</h4>
					<p>Clique na sessão Cadastro e preencha todos os campos</p>
				</div>
				
				<div className={cardstyle.card}>
					<h4>Como fazer cadastro?</h4>
					<p>Clique na sessão Cadastro e preencha todos os campos</p>
				</div>
				
				<div className={cardstyle.card}>
					<h4>Como fazer cadastro?</h4>
					<p>Clique na sessão Cadastro e preencha todos os campos</p>
				</div>
				
				<div className={cardstyle.card}>
					<h4>Como fazer cadastro?</h4>
					<p>Clique na sessão Cadastro e preencha todos os campos</p>
				</div>
				
				<div className={cardstyle.card}>
					<h4>Como fazer cadastro?</h4>
					<p>Clique na sessão Cadastro e preencha todos os campos</p>
				</div>
				
				<div className={cardstyle.card}>
					<h4>Como fazer cadastro?</h4>
					<p>Clique na sessão Cadastro e preencha todos os campos</p>
				</div>
				
				<div className={cardstyle.card}>
					<h4>Como fazer cadastro?</h4>
					<p>Clique na sessão Cadastro e preencha todos os campos</p>
				</div>
			</Scrollbars>

			<Button className={"col mt-4 " + unloggedstyle.footer_button} overwrite="background_gradient" icon="chevron-right">
				Não conseguiu esclarecer sua duvida? <span>Envie um email</span>
			</Button>
		</div>
	);
}
