//Packages
import React 	from 'react';
import { Link }	from "react-complete-router";

//Contexts
import systemcontext from '../../contexts/system';

//Components
import { Input, Form } 	from '../../components/Inputs';
import Button 			from "../../components/buttons";

//Styling
import cardstyle from "../../../css/modules/card.module.css";

export default function Register () {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//contexts
	const { register } = React.useContext(systemcontext);

    //-------------------------------------------------
    // Render
	//-------------------------------------------------

	return (
		<Form onSubmit={register}>
			<div className={cardstyle.card}>
				<Input 	name="name" 						label="NOME COMPLETO"		icon="user" 					/>
				<Input 	name="email" 						label="EMAIL"				icon="envelope" 				/>
				<Input 	name="password"						label="SENHA" 				icon="lock"		type="password" />
				<Input 	name="password_confirmation"		label="CONFIRME SUA SENHA" 	icon="lock"		type="password" />
			</div>
			
			<Button className="col" icon="chevron-right" type="submit">ENTRAR</Button>

			<p className="col-md-8 offset-md-2 pt-4">Ao criar uma conta, você concorda com nossos <Link to="terms">Termos de Serviço</Link> e <Link to="policy">Politica de Privacidade</Link></p>
		</Form>
	);
}