//Packages
import React, { Suspense }		from 'react';
import { Switch, Route, Link } 	from 'react-complete-router';
import Carousel 				from "nuka-carousel";

//Styling
import screenstyle from "../../../css/modules/views/unlogged.module.css";

//Views
import bundle from "./index";

export default function Unlogged () {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//states
	const [ current, setcurrent ] = React.useState(0);

	//-------------------------------------------------
	// Callbacks
	//-------------------------------------------------

	const onClick = React.useCallback((index) => {
		let value 	= index;
		value 		= value >= 0 ? value : 0;
		value 		= value <= 3 ? value : 3;

		setcurrent(value);
	}, []);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<div className={screenstyle.screen}>
			<div className="container">
				<Carousel
					enableKeyboardControls={false}
					withoutControls={true}
					className={screenstyle.header}
					slidesToShow={2}
					slideIndex={current}
				>
					<Link active={screenstyle.active} onClick={() => onClick(0)} to="/register">Cadastro</Link>
					<Link active={screenstyle.active} onClick={() => onClick(1)} to="/">Acessar</Link>
					<Link active={screenstyle.active} onClick={() => onClick(2)} to="/remember">Recuperar</Link>
					<Link active={screenstyle.active} onClick={() => onClick(3)} to="/logout">Sair</Link>
					<Link active={screenstyle.active} onClick={() => onClick(4)} to="/help">Ajuda</Link>
				</Carousel>
			</div>

			<div className="d-flex flex-column justify-content-center align-items-center mt-4">

				<div className="col-md-6">
					<Suspense fallback={""}>
						<Switch>
							<Route to={bundle.Remember} path="/remember"	title="Recuperar" 	/>
							<Route to={bundle.Register} path="/register" 	title="Cadastrar"	/>
							<Route to={bundle.Help} 	path="/help" 		title="Ajuda"		/>
							<Route to={bundle.Logout} 	path="/logout" 		title="Sair"		/>
							<Route to={bundle.Login} 						title="Entrar"		/>
						</Switch>
					</Suspense>
				</div>
			</div>
		</div>
	);
}
