//Packages
import React 	from 'react';

//Contexts
import systemcontext from '../../contexts/system';

//Components
import { Input, Form } 	from '../../components/Inputs';
import Button 			from "../../components/buttons";

//Styling
import cardstyle from "../../../css/modules/card.module.css";

export default function Remember () {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//contexts
	const { login } = React.useContext(systemcontext);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<Form onSubmit={login}>
			<p className="text-center">Digite o endereço de e-mail que você usou para criar sua conta e nós lhe enviaremos um link para redefinir sua senha</p>

			<div className={cardstyle.card}>
				<Input name="email" 	label="EMAIL" 	icon="user" />
			</div>
			
			<Button className="col" icon="chevron-right" type="submit">ENVIAR</Button>
		</Form>
	);
}
