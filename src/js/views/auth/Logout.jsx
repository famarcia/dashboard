//Packages
import React 	from 'react';

//Components
import Button from '../../components/buttons';

export default function Logout () {

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<div>
			<p className="text-center pb-4">Você pode sair da conta a qualquer momento, inclusive excluir, para isso vá em seu Perfil e clique em Excluir Conta.</p>

			<Button className="col" icon="chevron-right">SAIR</Button>
		</div>
	);
}
