//Packages
import React 	from 'react';

//Contexts
import systemcontext from '../../contexts/system';

//Components
import { Input, Form } 	from '../../components/Inputs';
import Button 			from "../../components/buttons";

//Styling
import cardstyle from "../../../css/modules/card.module.css";

export default function Login() {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//contexts
	const { login } = React.useContext(systemcontext);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<Form onSubmit={login}>
			<div className={cardstyle.card}>
				<Input name="email" 	label="CPF / E-MAIL" 	icon="user" />
				<Input name="password"	label="SENHA" 			icon="lock"	type="password" />
			</div>
			
			<Button className="col" icon="chevron-right" type="submit">ENTRAR</Button>
		</Form>
	);
}
