//Packages
import { lazy } from "react";

const bundle = {
	Login 		: lazy (() => import (/* webpackChunkName: "Login"		*/ "./Login").then((Component : any) => Component)),
	Register	: lazy (() => import (/* webpackChunkName: "Register"	*/ "./Register").then((Component : any) => Component)),
	Help		: lazy (() => import (/* webpackChunkName: "Help"		*/ "./Help").then((Component : any) => Component)),
	Logout		: lazy (() => import (/* webpackChunkName: "Logout"		*/ "./Logout").then((Component : any) => Component)),
	Remember	: lazy (() => import (/* webpackChunkName: "Remember"	*/ "./Remember").then((Component : any) => Component)),
};

export default bundle;