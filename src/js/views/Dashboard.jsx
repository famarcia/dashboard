//Packages
import React	from 'react';
import Default 	from '../components/charts/Default';
import Pie from '../components/charts/Pie';

export default function Dashboard () {

    //-------------------------------------------------
    // Render
	//-------------------------------------------------

    return (
		<div className="row">
			<h2 className="mb-5">Bem-vindo de volta</h2>

			<div className="row">
				<div className="col-md-6">
					<Default />
				</div>
				<div className="col-md-6">
					<Pie />
				</div>
			</div>
		</div>
    );
}
