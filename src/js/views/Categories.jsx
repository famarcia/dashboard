//Packages
import React		from 'react';
import { toast } 	from 'react-toastify';

//Components
import Complete from '../components/lists/Complete';

//Contexts
import systemcontext 	from '../contexts/system';
import { ModalContext } from '../contexts/modal';

//Modals
import Create from '../components/modals/Category/Create';
import Edit from '../components/modals/Category/Edit';

export default function Categories () {

    //-------------------------------------------------
    // Properties
	//-------------------------------------------------

	//states
	const editdata = React.useState({});

	//contexts
	const { user, request, refresh } 	= React.useContext(systemcontext);
	const { show } 						= React.useContext(ModalContext);

    //-------------------------------------------------
    // Callbacks
	//-------------------------------------------------

	const startEdit = React.useCallback((data) => {
		editdata[1](data);
		show("category-edit");
	}, []);

	const onDelete = React.useCallback((data) => {
		request.delete(user.pharmacy.id + "/categories/" + data.id)
		.then(() => {
			refresh();
			toast("Categoria apagada com sucesso");
		});
	}, []);

    //-------------------------------------------------
    // Render
	//-------------------------------------------------

    return (
		<div className="row">
			<Complete
				src={user.pharmacy.id + "/categories"}
				onAdd={() => show("category-create")}
				onEdit={startEdit}
				onDelete={onDelete}
			/>

			{/* Modals */}

			<Create 				/>
			<Edit 	data={editdata} />
		</div>
    );
}
