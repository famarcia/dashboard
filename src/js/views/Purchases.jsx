//Packages
import React	from 'react';

//Modules
import { Table, Header, Column, Action } from '../modules/Table';

export default function Purchases () {

    //-------------------------------------------------
    // Render
	//-------------------------------------------------

    return (
		<div className="table-responsive">
			<Table className="table table-borderless" paginate src={"purchases"}>
				<Header>
					<Column name="name">Nome</Column>
					<Column name="count">Itens</Column>
				</Header>

				<Action>
				</Action>
			</Table>
		</div>
    );
}
