//Packages
import React                        from 'react';
import Button from '../components/buttons';

export default function NotFound () {

    //-------------------------------------------------
    // Render
	//-------------------------------------------------

    return (
		<div id="app" className="app">
			<h1 className="text-center">404</h1>
			<p className="text-center">A página solicitada não pôde ser encontrada.</p>
		</div>
    );
}
