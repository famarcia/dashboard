//Packages
import React	from 'react';

//Contexts
import systemcontext 	from '../contexts/system';
import { ModalContext } from '../contexts/modal';

//Components
import Button 	from '../components/buttons';
import Form 	from '../components/Inputs/Form';
import Input 	from '../components/Inputs/Input';

//Modals
import WelcomePharmacy 			from '../components/modals/Welcome/Pharmacy';
import WelcomePharmacyCreate 	from '../components/modals/Welcome/PharmacyCreate';
import WelcomePharmacyJoin 		from '../components/modals/Welcome/PharmacyJoin';
import WelcomeMotoboy 			from '../components/modals/Welcome/Motoboy';

export default function Config () {

    //-------------------------------------------------
    // Properties
	//-------------------------------------------------

	//contexts
	const { user } = React.useContext(systemcontext);
	const { show } = React.useContext(ModalContext);

    //-------------------------------------------------
    // Callbacks
	//-------------------------------------------------

	const onUpdateProfile = React.useCallback((data) => {

	}, []);

    //-------------------------------------------------
    // Render
	//-------------------------------------------------

    return (
		<div className="col-md-6 offset-md-3 row">

			<div className="col-md-12">
				<h2>Atualizar usuário</h2>

				<Form onSubmit={onUpdateProfile}>
					<Input name="name" 	icon="user"		label="Nome completo"/>
					<Input name="email" icon="envelope" label="Email" type="email" />
				</Form>
			</div>

			<div className="col-md-12">
				<h2>Atualizar dados</h2>

				<Form onSubmit={onUpdateProfile}>
					<Input name="rg" 		icon="user"				label="RG" 	/>
					<Input name="cpf" 		icon="user" 			label="CPF" />
					<Input name="birthday" 	icon="birthday-cake" 	label="Data de nascimento" type="date" />
				</Form>
			</div>

			<div className="col-md-12">
				<h2>Atualizar endereço de entrega</h2>

				<Form onSubmit={onUpdateProfile}>
					<Input name="cep"	 		icon="map" 	label="CEP" 	/>
					<Input name="number" 		icon="map"	label="Numero" 	/>
					<Input name="street" 		icon="map"	label="Rua" 	/>
					<Input name="neighbor" 		icon="map"  label="Bairro" 	/>
					<Input name="city" 			icon="map" 	label="Cidade" 	/>
					<Input name="state" 		icon="map" 	label="Estado" 	/>
				</Form>
			</div>

			<div className="col-md-12 mt-4">
				<h2>Registros extras</h2>

				{
					!user.motoboy &&
					
					<Button className="mt-4 col" icon="motorcycle" onClick={() => show("introduction-motoboy")}>Registrar motoboy</Button>
				}

				{
					!user.pharmacy &&
					
					<Button className="mt-4 col" icon="prescription-bottle-alt" onClick={() => show("introduction-pharmacy")}>Registrar farmacia</Button>
				}

				{
					(user.pharmacy && user.motoboy) &&

					<p className="mt-4">Nenhum registro extra que possa ser feito no momento</p>
				}
			</div>

			<div className="col-md-12">
				<h2>Apagar conta</h2>

				<Button className="mt-4 col" icon="exclamation" color="danger">Apagar a sua conta permanentemente</Button>
			</div>
			
			{/* Modals */}

			<WelcomePharmacy 		/>
			<WelcomePharmacyCreate 	/>
			<WelcomePharmacyJoin 	/>
			<WelcomeMotoboy 		/>
		</div>
    );
}
