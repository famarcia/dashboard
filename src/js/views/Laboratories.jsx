//Packages
import React		from 'react';
import { toast } 	from 'react-toastify';

//Components
import Complete from '../components/lists/Complete';

//Contexts
import systemcontext 	from '../contexts/system';
import { ModalContext } from '../contexts/modal';

//Modals
import Create from '../components/modals/Laboratory/Create';
import Edit from '../components/modals/Laboratory/Edit';

export default function Laboratories () {

    //-------------------------------------------------
    // Properties
	//-------------------------------------------------

	//states
	const editdata = React.useState({});

	//contexts
	const { user, request, refresh } 	= React.useContext(systemcontext);
	const { show } 						= React.useContext(ModalContext);

    //-------------------------------------------------
    // Callbacks
	//-------------------------------------------------

	const startEdit = React.useCallback((data) => {
		editdata[1](data);
		show("laboratory-edit");
	}, []);

	const onDelete = React.useCallback((data) => {
		request.delete(user.pharmacy.id + "/laboratories/" + data.id)
		.then(() => {
			refresh();
			toast("Laboratório apagado com sucesso");
		});
	}, []);

    //-------------------------------------------------
    // Render
	//-------------------------------------------------

    return (
		<div className="row">
			<Complete
				src={user.pharmacy.id + "/laboratories"}
				onAdd={() => show("laboratory-create")}
				onEdit={startEdit}
				onDelete={onDelete}
			/>

			{/* Modals */}

			<Create 				/>
			<Edit 	data={editdata} />
		</div>
    );
}
