//Packages
import { lazy } from "react";

const bundle = {
	Dashboard 		: lazy (() => import (/* webpackChunkName: "Dashboard"		*/ "./Dashboard").then((Component : any) => Component)),
	Purchases 		: lazy (() => import (/* webpackChunkName: "Purchases"		*/ "./Purchases").then((Component : any) => Component)),
	Categories 		: lazy (() => import (/* webpackChunkName: "Categories"		*/ "./Categories").then((Component : any) => Component)),
	Laboratories	: lazy (() => import (/* webpackChunkName: "Laboratories"	*/ "./Laboratories").then((Component : any) => Component)),
	Products 		: lazy (() => import (/* webpackChunkName: "Products"		*/ "./Products").then((Component : any) => Component)),
	NotFound 		: lazy (() => import (/* webpackChunkName: "NotFound"		*/ "./NotFound").then((Component : any) => Component)),
	Config 			: lazy (() => import (/* webpackChunkName: "Config"			*/ "./Config").then((Component : any) => Component)),
};

export default bundle;