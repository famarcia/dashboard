//Packages
import React 	from 'react';

//Contexts
import systemcontext from '../../contexts/system';

//Styles
import loadingstyle from "../../../css/modules/views/loading.module.css";

export default function Loading () {

    //-------------------------------------------------
    // Properties
	//-------------------------------------------------

	const { loading } = React.useContext(systemcontext);

    //-------------------------------------------------
    // Render
	//-------------------------------------------------
	
	return (
		<div className={(loadingstyle.loading + " " + (loading ? "":loadingstyle.finished))} />
	);
}