//Packages
import React 					from 'react';
import { Text as FormText } 	from "formalization";

//Styling
import inputstyling 	from "../../../css/modules/components/inputs/input.module.css";

export default function Text (props) {

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<div className={inputstyling.input + " row " + (!props.icon? "pl-4":"")}>
			{
				props.icon &&
				
				<div className={inputstyling.icon}>
					<i className={"fa fa-" + props.icon} />
				</div>
			}
			<div className="col p-0">
				{props.label && <label>{props.label}</label>}
				<FormText {...props} />
			</div>
		</div>
	);
}