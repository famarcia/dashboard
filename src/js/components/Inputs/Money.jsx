//Packages
import React 					from 'react';
import { Input as FormInput } 	from "formalization";
import * as mask    			from "vanilla-masker";

//Styling
import inputstyling 	from "../../../css/modules/components/inputs/input.module.css";

export default function Money (props) {

    //-------------------------------------------------
    // Callbacks
    //-------------------------------------------------

    const onMaskMoney = React.useCallback((value) => {
        return mask.toMoney(value);
    }, []);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<div className={inputstyling.input + " row " + (!props.icon? "pl-4":"")}>
			{
				props.icon &&
				
				<div className={inputstyling.icon}>
					<i className={"fa fa-" + props.icon} />
				</div>
			}
			<div className="col p-0">
				{props.label && <label>{props.label}</label>}
				<FormInput filters={[onMaskMoney]} {...props} />
			</div>
		</div>
	);
}