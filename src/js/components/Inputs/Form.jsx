//Packages
import React 					from 'react';
import { Form as FormForm } 	from "formalization";

export default function Form ({children, ...props}) {

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<FormForm {...props}>
			{children}
		</FormForm>
	);
}