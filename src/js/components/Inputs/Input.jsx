//Packages
import React 					from 'react';
import { Input as FormInput } 	from "formalization";

//Styling
import inputstyling 	from "../../../css/modules/components/inputs/input.module.css";

export default function Input (props) {

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<div className={inputstyling.input + " row " + (!props.icon? "pl-4":"")}>
			{
				props.icon &&
				
				<div className={inputstyling.icon}>
					<i className={"fa fa-" + props.icon} />
				</div>
			}
			<div className="col p-0">
				{props.label && <label>{props.label}</label>}
				<FormInput {...props} />
			</div>
		</div>
	);
}