//Require components
import _Input 		from "./Input";
import _Form 		from "./Form";
import _Select		from "./Select";
import _SelectAsync	from "./Select/Async";

//Bundle all components
const bundle = {
	Input 		: _Input,
	Form 		: _Form,
	Select		: _Select,
	SelectAsync : _SelectAsync,
}

//Export
export default bundle;
export const Input 			= _Input;
export const Form 			= _Form;
export const Select 		= _Select;
export const SelectAsync 	= _SelectAsync;