//Packages
import React        from 'react';
import ReactSelect  from "react-select";

//Styling
import inputstyling 	from "../../../css/modules/components/inputs/input.module.css";

//Contexts
import { Wrapper }	from "formalization";

export default function Select ({value, label, icon, name, isMulti, children}) {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

    //States
    const [ _value, setvalue ] = React.useState(value);

	//-------------------------------------------------
	// Callbacks
	//-------------------------------------------------

	const cleanChange = React.useCallback(localvalue => {
		let newvalue;

		//Multiple select
		if (isMulti) {
			newvalue = localvalue.map(row => row.value);
		}
		//Fix the result
		else {
			newvalue = localvalue.value;
        }

        setvalue(newvalue);
	}, [isMulti]);

	//-------------------------------------------------
	// Memos
    //-------------------------------------------------

    const torawoptions = React.useMemo(() => {
        let response = [];

        if (!children) return [{value:"",label:"Não existem opções"}];

        React.Children.forEach(children, element => {
            if (typeof element.type === "string" && element.type === "option") {
                response.push({
                    value:element.props.value,
                    label:element.props.children
                });
            }
        });

        return response;
    }, [children]);

	const torawvalue = React.useMemo(() => {
        let   raw;

		//Simple select
		if ((typeof _value === 'string' && value.length) || typeof _value === "number") {
			raw = torawoptions.find(option => option.value === _value);
		}
		//Multiple select
		else if (isMulti && _value && ((_value instanceof Object || Array.isArray(_value)) && _value.length > 0)) {
			raw = _value.map(row => torawoptions.find(option => option.value === row));
		}
		//Not valid
		else {
			raw = null;
		}

		return raw;
	}, [torawoptions, _value, isMulti, value.length]);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<div className={inputstyling.input + " row " + (!icon? "pl-4":"")}>
			{
				icon &&
				
				<div className={inputstyling.icon}>
					<i className={"fa fa-" + icon} />
				</div>
			}
			<div className="col p-0">
				{label && <label>{label}</label>}
				<Wrapper value={_value} setValue={setvalue} name={name}>
					<ReactSelect
						value={torawvalue}
						options={torawoptions}
						onChange={cleanChange}
						isMulti={isMulti}
						placeholder="Escolha"
					/>
				</Wrapper>
			</div>
		</div>
	);
}
