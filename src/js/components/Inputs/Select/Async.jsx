//Packages
import React        from 'react';
import ReactSelect  from "react-select";


//Styling
import inputstyling 	from "../../../../css/modules/components/inputs/input.module.css";

//Contexts
import systemcontext	from "../../../contexts/system";
import { Wrapper }		from "formalization";

export default function Async ({src, name, isMulti, htmlLabel, icon, query = [], id = "id", label = "label"}) {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//contexts
	const { request }				= React.useContext(systemcontext);

    //States
    const [ value, setvalue ]       = React.useState("");
	const [ filter, setfilter ] 	= React.useState("");
	const [ options, setoptions ]	= React.useState([]);
    const [ timer, settimer ]   	= React.useState(null);
    const [ loading, setloading ]	= React.useState(true);
    const mounted                   = React.useRef(false);

	//-------------------------------------------------
	// Memos
	//-------------------------------------------------

	const querystring = React.useMemo(() => {
		return ((query && query.length > 0)? ("&" + query.join("&")):"");
	}, [query]);

	const torawvalue = React.useMemo(() => {
        let   raw;

		//Simple select
		if ((typeof value === 'string' && value.length) || typeof value === "number") {
			raw = options.find(option => option.value === value);
		}
		//Multiple select
		else if (isMulti && value && ((value instanceof Object || Array.isArray(value)) && value.length > 0)) {
			raw = value.map(row => options.find(option => option.value === row));
		}
		//Not valid
		else {
			raw = null;
        }

		return raw;
	}, [options, value, isMulti]);

	//-------------------------------------------------
	// Callbacks
	//-------------------------------------------------

	const cleanChange = React.useCallback(localvalue => {
		let newvalue;

		//Multiple select
		if (isMulti) {
			newvalue = localvalue.map(row => row.value);
		}
		//Fix the result
		else {
			newvalue = localvalue.value;
        }

        setvalue(newvalue);
	}, [setvalue, isMulti]);

	const onFilter = React.useCallback((row, input) => {
	    setfilter(input);

		//We are filtering by the api, so we will return true always
		return true;
	}, []);

	const onStart = React.useCallback(() => {
		setloading(true);

        //Cancel other callbacks
        if (timer) {
            clearTimeout(timer);
        }

        //Component still alive
        mounted.current = true;

        //Send value change
        settimer(setTimeout (() => {
            //Tell that the value has changed
			request.get(src + "?perpage=25&search=" + filter + querystring, {
				wait: false,
				toastify: false
			})
			.then(res => {
				let data = [];

				//Parse the data
				for (let i = 0; i < res.data.data.length; i++) {
					const row = res.data.data[i];

					data.push({value:row[id], label:row[label]});
				}

				//There is a bug with react select when there are no options
				//Filter is not updated anymore, so let's pretend that there
				//always is
				if (data.length === 0) {
					data.push({value:null,disabled:true,label:"Não existem opções"});
				}

                if (mounted.current) {
                    setoptions(data);
                    setloading(false);
                }
			});

            //Clear the timer
            settimer(null);
        }, 500));
	}, [filter, src, request, id, label, querystring, timer]);

	//-------------------------------------------------
	// Effects
	//-------------------------------------------------

	React.useEffect(() => {
		if (mounted.current === false) onStart();

        //Prevent ajax from setting things on unmounted components
        return () => {
            mounted.current = false;
        }
	}, []);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<div className={inputstyling.input + " row " + (!icon? "pl-4":"")}>
			{
				icon &&
				
				<div className={inputstyling.icon}>
					<i className={"fa fa-" + icon} />
				</div>
			}
			<div className="col p-0">
				{htmlLabel && <label>{htmlLabel}</label>}
				<Wrapper value={value} setValue={setvalue} name={name}>
					<ReactSelect
						value={torawvalue}
						isLoading={loading}
						options={options}
						onChange={cleanChange}
						filterOption={onFilter}
						isMulti={isMulti}
						placeholder="Selecione"
					/>
				</Wrapper>
			</div>
		</div>
	);
}
