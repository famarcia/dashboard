//Packages
import React 	from "react";
import { Link } from "react-complete-router";

//Styling
import navbar from "../../../css/modules/components/bar/navbar.module.css";

export default function Navlink (props) {
	return (
		<Link to={props.to} active={navbar.active}>
			<i className={"fa fa-" + props.icon} />
			<span className={navbar.close + " pl-2"}>
				{props.children}
			</span>
		</Link>
	);
}