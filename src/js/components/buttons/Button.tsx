//Packages
import React from "react";
import { Link } from "react-complete-router";

//Styling
import styling  from "../../../css/modules/components/button/default.module.css";
import colors   from "../../../css/modules/color.module.css";

//Interface
import iDefault from "../../interfaces/components/button/iDefault";

export default function Default ({icon, group, to = null, size = 1, color = "primary", background = "background", children, href = null, overwrite = "", ...props} : iDefault) {

	//-------------------------------------------------
	// Memos
    //-------------------------------------------------

    const className = React.useMemo(() : string => {
        let response = styling.button;

        //Optional classes
        if(props.className) response += " " + props.className;
        if(group)           response += " " + styling.group;
		if(icon)            response += " " + styling.icon;
		if(overwrite)		response += " " + colors[overwrite];
        else if(color)      response += " " + colors[background + "_color_" + color];

        return response;
    }, [props.className, group, icon, color, background, overwrite]);

	//-------------------------------------------------
	// Render
    //-------------------------------------------------

    return React.createElement(to? Link:(href? "a":"button"), {
		type:"button",
        ...props,
        className:className,
        ...(props.active? {
            active:colors.active
        }: {}),
        ...(to? {
            to:to
        }: {}),
        ...(href? {
            href:href,
            target:"_blank",
            rel:"noopener noreferrer"
        }:{})
    }, [
		children,
		icon ? <i key={"yay"} className={"fa fa-" + icon}/>:null
    ]);
}