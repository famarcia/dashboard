//Packages
import React 		from 'react';
import Cookie		from "js-cookie";
import { toast }	from "react-toastify";

//Components
import App from "./App";

//Contexts
import SystemContext 	from "../contexts/system";

//Interfaces
import { iSystem }			from '../contexts/system';
import iUser 				from '../interfaces/user';
import { iLoginResponse } 	from "../interfaces/responses";
import Loading 				from '../views/general/Loading';
import ModalContainer from '../contexts/modal';

export default function System ({csrf = true}) {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//States
	const [ user, setuser ] 				= React.useState<iUser | boolean | undefined>(undefined);
	const [ token, settoken ]				= React.useState<string | undefined>(Cookie.get("token"));
	const [ loading, setloading ]			= React.useState(true);
	const [ waiting, setwaiting ]			= React.useState(true);
	const [ shouldRefresh, updateFefresh ]	= React.useState(0);

	//-------------------------------------------------
	// Memos
	//-------------------------------------------------

	const request = React.useMemo(() => {
		window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
		window.axios.interceptors.response.use(undefined, (error) => {
			if (error.response) {
				//Unauthentication
				if (error.response.status === 401) {

				}
				else {
					const data = error.response.data;
	
					//Validation error
					if (data.errors) {
						for (const error in data.errors) {
							if (error) {
								toast.warn(data.errors[error][0]);
								break;
							}
						}
					}
				}

				return Promise.reject(error);
			}
			else if (error.request) {
				toast.error("Algum erro interno ocorreu, tente novamente mais tarde.");

				return Promise.reject(error);
			}
			else {
				toast.error("Algum erro interno ocorreu, tente novamente mais tarde.");

				return Promise.reject(error);
			}
		});
		return window.axios;
	}, [token]);

	//-------------------------------------------------
	// Callbacks
	//-------------------------------------------------

	const login = React.useCallback((data : Object) => {
		//Resolve user against API
		return request.post<iLoginResponse>("login", data).then(({ data }) => {
			//Token
			Cookie.set("token", data.token);
			settoken(data.token);

			//User
			setuser(data.data as iUser);
		});
	}, [request]);

	const register = React.useCallback((data : Object) => {
		//Resolve user against API
		return request.post<iLoginResponse>("register", data).then(({ data }) => {
			//Token
			Cookie.set("token", data.token);
			settoken(data.token);

			//User
			setuser(data.data as iUser);
		}).catch(() => {
			
		});
	}, [request]);

	const logout = React.useCallback(() => {
		//Token
		Cookie.remove("token");
		settoken(undefined);
		setuser(undefined);
	}, []);

	const logged = React.useCallback(() => {
		//Resolve user against API
		if (csrf === true) {
			return request.get("sanctum/csrf-cookie")
			.then(response => {
				return request.get("/user")
				.then(response => {
					if (response.data.data) setuser(response.data.data);
					else setuser(false);

					setloading(false);
				}).catch(() => {
					setloading(false);
				});
			});
		}
		else {
			return request.get("/user")
			.then(response => {
				if (response.data.data) setuser(response.data.data);
				else setuser(false);	

				setloading(false);
			}).catch(() => {
				setloading(false);
			});
		}
	}, [request, csrf]);

	const refetch = React.useCallback(() => {
		return request.get("/user")
		.then(response => {
			if (response.data.data) setuser(response.data.data);
			else setuser(false);	

			setloading(false);
		}).catch(() => {

		});
	}, [request]);

	const setfakeuser = React.useCallback(() => {
		setuser({id:"asdas23", name:"Rafael Corrêa Chaves", email:"rafael.chaves98@hotmail.com"});
	}, []);

	const refresh = React.useCallback(() => {
		setTimeout(() => {
			const newvalue = shouldRefresh > 10 ? 0: (shouldRefresh + 1);
			updateFefresh(newvalue);
		}, 500);
	}, [shouldRefresh]);

	//-------------------------------------------------
	// Effects
	//-------------------------------------------------

	React.useEffect(() => {
		if (loading) logged();
	}, [loading, logged]);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	//context
	let context : iSystem = {login, logout, logged, request, user, setuser, register, loading, waiting, refetch, refresh, shouldRefresh};

	if (process.env.NODE_ENV === "development")
		context = {...context,setfakeuser};

	return (
        <SystemContext.Provider value={context}>
			<ModalContainer>
				<App />
				<Loading />
			</ModalContainer>
        </SystemContext.Provider>
	);
}
