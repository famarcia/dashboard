//Packages
import React                        from 'react';

//Components
import Item from "./itens/Complete";

//contexts
import systemcontext from '../../contexts/system';
import Button from '../buttons';

export default function Complete ({src, onAdd, onEdit, onDelete}) {

    //-------------------------------------------------
    // Properties
	//-------------------------------------------------

	//states
	const [ list, setlist ] = React.useState([]);

	//contexts
	const { request, shouldRefresh } = React.useContext(systemcontext);

    //-------------------------------------------------
    // Effects
	//-------------------------------------------------

	React.useEffect(() => {
		request.get(src)
		.then(response => {
			if (response.data.data) setlist(response.data.data);
		});
	}, [src, shouldRefresh]);

    //-------------------------------------------------
    // Render
	//-------------------------------------------------

    return (
		<React.Fragment>

			{/* Commands */}

			<Button icon="plus" onClick={onAdd} className="col-md-3">Adicionar</Button>

			{/* Table */}

			<div className="row">
				{list.map((item) => {
					return (
						<Item {...item} key={item.id}>
							<Button icon="edit" 	color="secondary"	className="col-md-3 mr-md-3 mb-3" 	onClick={() => onEdit(item)}>Editar</Button>
							<Button icon="times" 	color="danger" 		className="col-md-3" 		onClick={() => onDelete(item)}>Excluir</Button>
						</Item>
					);
				})}
			</div>
		</React.Fragment>
    );
}
