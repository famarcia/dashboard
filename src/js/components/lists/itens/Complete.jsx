//Packages
import React	from 'react';

//Styling
import liststyling from "../../../../css/modules/components/list/complete.module.css";

//Images
import defaultImage from "../../../../img/default-image.jpg";

export default function Complete (item) {

    //-------------------------------------------------
    // Memos
	//-------------------------------------------------

	const image = React.useMemo(() => {
		if (item.image)
			return item.image;
		else
			return defaultImage;
	}, [item.image]);

    //-------------------------------------------------
    // Render
	//-------------------------------------------------

    return (
		<div className={"row m-0 mt-4 mb-2 " + liststyling.item}>
			<div className={"col-md-3 " + liststyling.item_image}>
				<img alt="item image" className="img-fluid" src={image} />
			</div>

			<div className={"col-md-9 " + liststyling.content}>
				<h3>{item.label}</h3>
				<div className="row">
					<p>{item.description}</p>
				</div>
				<div className="row m-0 pb-4">
					{item.children}
				</div>
			</div>
		</div>
    );
}
