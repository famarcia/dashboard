//Packages
import React 	from 'react';

//Interfaces
import { iReactProps } from 'react-complete-router/dist/interfaces/iReact';

//Components
import Navbar 	from './menus/Navbar';
import Upperbar from './menus/Upperbar';

//Contexts
import systemcontext from '../contexts/system';

export default function DefaultLayout(props : iReactProps) {

    //-------------------------------------------------
    // Properties
	//-------------------------------------------------

	//states
	const [ navopen, setnavopen ] = React.useState(true);

	//contexts
	const { user }	= React.useContext(systemcontext);

    //-------------------------------------------------
    // Render
	//-------------------------------------------------

	//Don't return if there is not user
	if (!user) return null;
	
	return (
		<div style={{minHeight:"100vh",margin:0}} className="row">
			<Navbar 	status={navopen} setstatus={setnavopen} />
			<div className="col p-0">
				<Upperbar 	status={navopen} setstatus={setnavopen} />
				<div className="container mt-5 pt-5">
					<div id="content" className="pb-5 pl-md-3">
						<React.Suspense fallback={""}>
							{props.children}
						</React.Suspense>
					</div>
				</div>
			</div>
		</div>
	);
}