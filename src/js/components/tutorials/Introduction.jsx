//Packages
import React                        from 'react';

//Modals
import Welcome 					from '../modals/Welcome/Welcome';
import WelcomeMotoboy 			from '../modals/Welcome/Motoboy';
import WelcomePharmacy 			from '../modals/Welcome/Pharmacy';
import WelcomePharmacyCreate 	from '../modals/Welcome/PharmacyCreate';
import WelcomePharmacyJoin 		from '../modals/Welcome/PharmacyJoin';

//Contexts
import { ModalContext } from '../../contexts/modal';

export default function Introduction () {

    //-------------------------------------------------
    // Properties
    //-------------------------------------------------

	//contexts
	const { show } = React.useContext(ModalContext);

    //-------------------------------------------------
    // Effects
	//-------------------------------------------------

	React.useEffect(() => {
		show("introduction");
	}, []);

    //-------------------------------------------------
    // Render
	//-------------------------------------------------

    return (
		<React.Fragment>
			<Welcome 				/>
			<WelcomeMotoboy 		/>
			<WelcomePharmacy 		/>
			<WelcomePharmacyCreate 	/>
			<WelcomePharmacyJoin 	/>
		</React.Fragment>
    );
}
