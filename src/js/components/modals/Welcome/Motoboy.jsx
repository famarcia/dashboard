//Packages
import * as React 	from 'react';
import { toast } 	from 'react-toastify';

//Components
import Modal	from "../Modal";
import Button 	from '../../buttons';
import Form 	from '../../Inputs/Form';
import Input 	from '../../Inputs/Input';

//Contexts
import { ModalContext }	from "../../../contexts/modal";
import systemcontext 	from '../../../contexts/system';

export default function WelcomeMotoboy () {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//contexts
	const { hide, show }		= React.useContext(ModalContext);
	const { request, refetch }	= React.useContext(systemcontext);

	//-------------------------------------------------
	// Callbacks
	//-------------------------------------------------

	const onSubmit = React.useCallback((data) => {
		request.post("user/motoboy", data)
		.then(answer => {
			if (answer.data.data) {
				refetch();
				hide();
				toast("Registro de motoboy criado com sucesso");
			}
		});
	}, [request]);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<Form onSubmit={onSubmit}>
			<Modal.Modal name="introduction-motoboy" >
				<Modal.Title>Cadastro de motoboys</Modal.Title>

				<Modal.Content>
					<Input label="RG" 			name="rg" 	/>
					<Input label="CPF" 			name="cpf" 	/>
					<Input label="CNPJ" 		name="cnpj" />
					<Input label="Nascimento" 	name="birthday" type="date" />
				</Modal.Content>

				<Modal.Footer>
					<Button className="col-12 mb-4"	icon="times" type="submit">Criar</Button>
					<Button className="col-12 mb-4"	icon="times" color="secondary" 	onClick={() => show("introduction")}>Voltar</Button>
				</Modal.Footer>
			</Modal.Modal>
		</Form>
	);
}
