//Packages
import * as React 	from 'react';

//Components
import Modal	from "../Modal";
import Button 	from '../../buttons';

//Contexts
import { ModalContext }	from "../../../contexts/modal";

export default function Welcome () {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//contexts
	const { hide, show } = React.useContext(ModalContext);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<Modal.Modal name="introduction" >
			<Modal.Title>Bem-vindo</Modal.Title>

			<Modal.Content>
				<h3>Bem-vindo ao farmaconline, gostaria de se cadastrar como motoboy ou farmacia?</h3>
			</Modal.Content>

			<Modal.Footer>
				<Button className="col-12 mb-4" icon="motorcycle"				onClick={() => show("introduction-motoboy")}>Cadastrar como motoboy</Button>
				<Button className="col-12 mb-4" icon="prescription-bottle-alt"	onClick={() => show("introduction-pharmacy")}>Cadastrar como farmacia</Button>
				<Button className="col-12" 		icon="times" color="secondary" 	onClick={hide}>Fechar</Button>
			</Modal.Footer>
		</Modal.Modal>
	);
}
