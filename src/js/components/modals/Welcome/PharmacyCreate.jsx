//Packages
import * as React 	from 'react';
import { toast } 	from 'react-toastify';

//Components
import Modal		from "../Modal";
import Button 		from '../../buttons';
import Form 		from '../../Inputs/Form';
import Input 		from '../../Inputs/Input';
import SelectAsync 	from '../../Inputs/Select/AsyncCreatable';

//Contexts
import { ModalContext }	from "../../../contexts/modal";
import systemcontext 	from '../../../contexts/system';

export default function WelcomePharmacyCreate () {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//contexts
	const { hide, show }		= React.useContext(ModalContext);
	const { request, refetch }	= React.useContext(systemcontext);

	//-------------------------------------------------
	// Callbacks
	//-------------------------------------------------

	const onSubmit = React.useCallback((data) => {
		request.post("user/pharmacy", data)
		.then(answer => {
			if (answer.data.data) {
				refetch();
				hide();
				toast("Registro de farmacia criado com sucesso");
			}
		});
	}, [request]);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<Form onSubmit={onSubmit}>
			<Modal.Modal name="introduction-pharmacy-create" >
				<Modal.Title>Cadastro de farmacia</Modal.Title>

				<Modal.Content>
					<SelectAsync 	htmlLabel="Marca"	name="brand_id" src="brands"	/>
					<Input 			label="Nome" 		name="label" 	/>
					<Input 			label="CRF" 		name="crf" 		/>
					<Input 			label="CNPJ" 		name="cnpj" 	/>
				</Modal.Content>

				<Modal.Footer>
					<Button className="col-12 mb-4"	icon="times" type="submit">Criar</Button>
					<Button className="col-12 mb-4"	icon="times" color="secondary" 	onClick={() => show("introduction-pharmacy")}>Voltar</Button>
					<Button className="col-12" 		icon="times" color="secondary" 	onClick={hide}>Cancelar</Button>
				</Modal.Footer>
			</Modal.Modal>
		</Form>
	);
}
