//Packages
import * as React 	from 'react';
import { toast } 	from 'react-toastify';

//Components
import Modal	from "../Modal";
import Button 	from '../../buttons';
import Form 	from '../../Inputs/Form';

//Contexts
import { ModalContext }	from "../../../contexts/modal";
import systemcontext 	from '../../../contexts/system';

export default function WelcomePharmacyJoin () {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//contexts
	const { hide, show }		= React.useContext(ModalContext);
	const { request, refetch }	= React.useContext(systemcontext);

	//-------------------------------------------------
	// Callbacks
	//-------------------------------------------------

	const onSubmit = React.useCallback((data) => {
		request.post("user/motoboy", data)
		.then(answer => {
			if (answer.data.data) {
				refetch();
				hide();
				toast("Registro de motoboy criado com sucesso");
			}
		});
	}, [request]);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<Form onSubmit={onSubmit}>
			<Modal.Modal name="introduction-pharmacy-join" >
				<Modal.Title>Entrar em farmacia existente</Modal.Title>

				<Modal.Content>
					<h2>Não implementado ainda</h2>
				</Modal.Content>

				<Modal.Footer>
					<Button className="col-12 mb-4"	icon="times" color="secondary" 	onClick={() => show("introduction-pharmacy")}>Voltar</Button>
					<Button className="col-12" 		icon="times" color="secondary" 	onClick={hide}>Cancelar</Button>
				</Modal.Footer>
			</Modal.Modal>
		</Form>
	);
}
