//Packages
import * as React 	from 'react';

//Components
import Modal	from "../Modal";
import Button 	from '../../buttons';

//Contexts
import { ModalContext }	from "../../../contexts/modal";

export default function WelcomePharmacy () {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//contexts
	const { hide, show }		= React.useContext(ModalContext);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<Modal.Modal name="introduction-pharmacy" >
			<Modal.Title>Cadastro de farmacia</Modal.Title>

			<Modal.Content>
				
			</Modal.Content>

			<Modal.Footer>
				<Button className="col-12 mb-4"	icon="times" onClick={() => show("introduction-pharmacy-create")}>Cadastrar farmacia</Button>
				<Button className="col-12 mb-4"	icon="times" onClick={() => show("introduction-pharmacy-join")}>Entrar em farmacia existente</Button>
				<Button className="col-12 mb-4"	icon="times" color="secondary" 	onClick={() => show("introduction")}>Voltar</Button>
			</Modal.Footer>
		</Modal.Modal>
	);
}
