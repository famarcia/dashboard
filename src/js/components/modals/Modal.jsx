//Core
import * as React 			from "react";

//Contexts
import { ModalContext }		from "../../contexts/modal";

//Styles
import cardstyle 			from "../../../css/modules/components/modal.module.css";

//-------------------------------------------------
// index
//-------------------------------------------------
const Data = {
	Modal: 		ModalBase,
	Title: 		Title,
	Content: 	Content,
	Footer: 	Footer,
};

export default Data;

//-------------------------------------------------
// Main
//-------------------------------------------------

export function ModalBase (props) {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//contexts
	const { display, hide, addModal, removeModal, exists }	= React.useContext(ModalContext);

	//-------------------------------------------------
	// Effects
	//-------------------------------------------------

	//Build modal object
	let title 	= null;
	let content = null;
	let footer 	= null;

	//Find important parts
	React.Children.map(props.children, (child, i) => {
		//Not a react type of child
			switch (child.type.name) {
				case "Title":
					title 	= child;
				break;
				default:
				case "Content":
					content = child;
				break;
				case "Footer":
					footer 	= child;
				break;
		}
	});

	React.useEffect(() => {
        //Check if modal already exists
        if (exists(props.name)) return;

		//Add modal on mount
		addModal({
			//Extra settings
			className: 	props.className,
			large: 		props.large,
			closeable: 	props.closeable,
			options: 	props.options,
			fullscreen: props.fullscreen,
			name: 		props.name
		});

		//Remove modal on unmount
		return () => {
			removeModal(props.name);
		}
	});

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<div
			className={"fullscreen pt-5 img-dark " + (display === props.name ? "fade-in":"fade-out")}
			id="modal-container"
		>
			<div className="container pt-5">
				<div className="card">
					<div className={"card-header " + cardstyle.header}>
						{title}

						<div className="card-options">
							{props.options}
							
							{/* Close button */}
							<button type="button" className={"close " + (props.closeable !== false? "":"hide")} onClick={hide} aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
					
					{content}
					{footer}
				</div>
			</div>
		</div>
	);
}

export function Title (props) {
	return <h2 className="p-0 m-0">{props.children}</h2>;
}

export function Content (props) {
	return <div className={"card-body " + (props.className? props.className:"")}>{props.children}</div>;
}

export function Footer (props) {
	return <div className={"card-bottom row pl-3 pr-3 " + (props.className? props.className:"")}>{props.children}</div>;
}
