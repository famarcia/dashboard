//Packages
import * as React 	from 'react';
import { toast } 	from 'react-toastify';

//Components
import Modal	from "../Modal";
import Button 	from '../../buttons';
import Form 	from '../../Inputs/Form';
import Input 	from '../../Inputs/Input';
import Text 	from "../../Inputs/Text";

//Contexts
import { ModalContext }	from "../../../contexts/modal";
import systemcontext 	from '../../../contexts/system';

export default function Edit ({data,onEdit = () => {}}) {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//contexts
	const { hide }						= React.useContext(ModalContext);
	const { request, user, refresh }	= React.useContext(systemcontext);

	//-------------------------------------------------
	// Callbacks
	//-------------------------------------------------

	const onSubmit = React.useCallback(() => {
		onEdit(
			request.patch(user.pharmacy.id + "/laboratories/" + data[0].id, data[0])
			.then(answer => {
				if (answer.data.data) {
					hide();
					toast("Editado com sucesso");
					refresh();
				}
			})
		);
	}, [request, hide, onEdit, user.pharmacy.id]);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<Form onSubmit={onSubmit} data={data}>
			<Modal.Modal name="laboratory-edit" >
				<Modal.Title>Editar laboratório</Modal.Title>

				<Modal.Content>
					<Input 	label="Código" 		name="code" 		/>
					<Input 	label="Título/Nome" name="label" 		/>
					<Text 	label="Descrição"	name="description"	/>
				</Modal.Content>

				<Modal.Footer>
					<Button className="col-12 mb-4"	icon="times" type="submit">Editar</Button>
					<Button className="col-12 mb-4"	icon="times" color="secondary" 	onClick={hide}>Voltar</Button>
				</Modal.Footer>
			</Modal.Modal>
		</Form>
	);
}
