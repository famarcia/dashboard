//Packages
import * as React 	from 'react';
import { toast } 	from 'react-toastify';

//Components
import Modal	from "../Modal";
import Button 	from '../../buttons';
import Form 	from '../../Inputs/Form';
import Input 	from '../../Inputs/Input';
import Text 	from "../../Inputs/Text";
import Money 	from '../../Inputs/Money';
import Async 	from '../../Inputs/Select/Async';

//Contexts
import { ModalContext }	from "../../../contexts/modal";
import systemcontext 	from '../../../contexts/system';

export default function Create ({onCreate = () => {}}) {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//contexts
	const { hide }						= React.useContext(ModalContext);
	const { request, user, refresh }	= React.useContext(systemcontext);

	//-------------------------------------------------
	// Callbacks
	//-------------------------------------------------

	const onSubmit = React.useCallback((data) => {
		onCreate(
			request.post(user.pharmacy.id + "/products", data)
			.then(answer => {
				if (answer.data.data) {
					hide();
					toast("Adicionado com sucesso");
					refresh();
				}
			})
		);
	}, [request, hide, onCreate, user.pharmacy.id]);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return (
		<Form onSubmit={onSubmit}>
			<Modal.Modal name="product-create" >
				<Modal.Title>Cadastrar produto</Modal.Title>

				<Modal.Content>
					<Async	htmlLabel="Categoria"	name="category_id"	src={user.pharmacy.id + "/categories"}	/>
					<Input 	label="Título/Nome" 	name="label" 		/>
					<Input 	label="Código" 			name="code" 		/>
					<Money 	label="Preço" 			name="cost" 		/>
					<Text 	label="Descrição"		name="description"	/>
				</Modal.Content>

				<Modal.Footer>
					<Button className="col-12 mb-4"	icon="times" type="submit">Criar</Button>
					<Button className="col-12 mb-4"	icon="times" color="secondary" 	onClick={hide}>Voltar</Button>
				</Modal.Footer>
			</Modal.Modal>
		</Form>
	);
}
