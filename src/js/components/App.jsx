//Packages
import React                        from 'react';
import { Router, Route, Switch }    from "react-complete-router";
import { ToastContainer } 			from 'react-toastify';
import Cookie 						from "js-cookie";

//Views
import bundle 	from "../views/index";
import Unlogged from "../views/auth/Unlogged";

//Components
import Dashport 	from "./Dashport";
import Introduction from './tutorials/Introduction';

//Contexts
import systemcontext 	from '../contexts/system';
import AppContext 		from '../contexts/app';
import { ModalContext } from '../contexts/modal';

//Guards
import titleguard from '../guards/title';

export default function App() {

    //-------------------------------------------------
    // Properties
    //-------------------------------------------------

	//contexts
	const { user } = React.useContext(systemcontext);
	const { show } = React.useContext(ModalContext);

	//states
	const [ title, settitle ] 								= React.useState("");
	const [ displayintroduction, setdisplayintroduction ] 	= React.useState(false);

    //-------------------------------------------------
    // Effects
    //-------------------------------------------------

	React.useEffect(() => {
		if (!user || !user.id) return;

		const firsttime = Cookie.get("firstime");

		if (!firsttime) {
			Cookie.set("firstime", true);
			setdisplayintroduction(true);
		}
	}, [user]);

    //-------------------------------------------------
    // Render
	//-------------------------------------------------

    return (
		<AppContext.Provider value={{title}}>
			<Router auth={user} basetitle={["Farma"," - "]} guards={{title:titleguard}} context={{title,settitle}}>

				<Switch>
					{/* Authenticated */}
					<Dashport logged>
						<Switch>
							{/* Configuration */}
							<Route to={bundle.Config} title="Configurações"	path="/config" 	/>

							{/* Resources */}

							<Route to={bundle.Purchases} 	title="Compras"			path="/purchases" 		/>
							<Route to={bundle.Categories} 	title="Categorias"		path="/categories" 		/>
							<Route to={bundle.Laboratories} title="Laboratórios"	path="/laboratories" 	/>
							<Route to={bundle.Products} 	title="Produtos"		path="/products" 		/>

							{/* Base */}

							<Route to={bundle.Dashboard} title="Dashboard"	path="/" exact 		/>
							<Route to={bundle.NotFound} title="Não encontrado"					/>
						</Switch>
					</Dashport>
					

					{/* Unauthenticated */}
					<Route guest  to={Unlogged} />
				</Switch>
			</Router>

			<ToastContainer />
			{displayintroduction && <Introduction />}
		</AppContext.Provider>
    );
}
