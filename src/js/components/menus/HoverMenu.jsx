//Packages
import React                        from 'react';

//Styling
import hoverstyling from "../../../css/modules/components/hover.module.css";

//Components
import Navlink from '../buttons/Navlink';

//Contexts
import systemcontext from '../../contexts/system';

export default function HoverMenu ({display, setdisplay}) {

    //-------------------------------------------------
    // Properties
	//-------------------------------------------------

	//contexts
	const { user, logout } = React.useContext(systemcontext);

    //-------------------------------------------------
    // Render
	//-------------------------------------------------

    return (
		<div className={hoverstyling.menu + (display ? (" " + hoverstyling.display):"")}>
			<Navlink to="/config" icon="cog" className="col">Configurações</Navlink>
			{
				user.pharmacy &&

				<Navlink to="/config/pharmacy" icon="prescription-bottle-alt" className="col">Farmacia</Navlink>
			}
			{
				user.motoboy &&

				<Navlink to="/config/motoboy" icon="motorcycle" className="col">Motoboy</Navlink>
			}
			<button onClick={logout}>Sair</button>
		</div>
    );
}
