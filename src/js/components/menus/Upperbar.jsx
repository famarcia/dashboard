//Packages
import React 	from 'react';

//Styling
import upperbarstyling 	from "../../../css/modules/components/bar/upperbar.module.css";
import profilestyling 	from "../../../css/modules/components/profile.module.css";

//Contexts
import appcontext 		from '../../contexts/app';
import systemcontext 	from '../../contexts/system';

//Imgs
import avatardefault 	from "../../../img/default-avatar.jpg";

//Components
import HoverMenu		from "./HoverMenu";

export default function Upperbar (props) {

    //-------------------------------------------------
    // Properties
	//-------------------------------------------------

	//states
	const [ display, setdisplay ] = React.useState(false);

	//contexts
	const { title } = React.useContext(appcontext);
	const { user }	= React.useContext(systemcontext);

    //-------------------------------------------------
    // Callbacks
	//-------------------------------------------------

	const onClick = React.useCallback(() => {
		props.setstatus(!props.status);
	}, [props]);

	const changeDisplay = React.useCallback(() => {
		setdisplay(!display);
	}, [display]);

    //-------------------------------------------------
    // Memos
	//-------------------------------------------------

	const avatar = React.useMemo(() => {
		if (user.avatar)
			return user.avatar;
		else
			return avatardefault;
	}, [user]);

	const usertitle = React.useMemo(() => {
		if (user.pharmacy)
			return user.pharmacy.label;
		else if (user.motoboy)
			return "motoboy " + user.name;
		else
			return user.name; 
	}, [user]);

    //-------------------------------------------------
    // Render
	//-------------------------------------------------
	
	return (
		<div className={upperbarstyling.upperbar}>
			<div className="container">
				<div className="row">
					<div className="offset-lg-4 offset-xl-0 col">
						<button className="pl-4 pr-4 mr-4" onClick={onClick}><i className="fa fa-bars" /></button>
						<span>{title}</span>
					</div>
					
					<div className="col">
						<button className="float-right mr-4" onClick={changeDisplay}>
							<span className="mr-2 d-none d-md-inline">{usertitle}</span>
							<img alt="profile avatar" className={profilestyling.profile} src={avatar} />
						</button>
						<HoverMenu display={display} />
					</div>
				</div>
			</div>
		</div>
	);
}