//Packages
import React 	from 'react';

//Styling
import navbar from "../../../css/modules/components/bar/navbar.module.css";

//Contexts
import systemcontext 	from '../../contexts/system';

//Components
import Navlink 			from '../buttons/Navlink';

export default function Navbar (props) {

    //-------------------------------------------------
    // Properties
	//-------------------------------------------------

	//contexts
	const { user } = React.useContext(systemcontext);

    //-------------------------------------------------
    // Callbacks
	//-------------------------------------------------

	const onClick = React.useCallback(() => {
		props.setstatus(!props.status);
	}, [props]);

    //-------------------------------------------------
    // Render
	//-------------------------------------------------
	
	return (
		<div className={(navbar.navbar + " " + (props.status ? "":navbar.closed))}>
			<div className={`${navbar.header}`}>
				<div>
					<i className="fa fa-pills mr-4" /> <span className={navbar.close}>FARMA</span>
					<button className={navbar.ham + " float-right"} onClick={onClick}>
						<i className="fa fa-bars" />
					</button>
					<h4 className={navbar.close}>Bem-vindo, {user.name}</h4>
				</div>
			</div>

			<div className={navbar.links}>
				<Navlink to="/" icon="tachometer-alt">Dashboard</Navlink>
				<Navlink to="/purchases" icon="shopping-cart">Compras</Navlink>
				{
					user.motoboy &&
					
					<Navlink to="/deliveries" icon="motorcycle">Entregas</Navlink>
				}
				{
					user.pharmacy &&
					
					<React.Fragment>
						<Navlink to="/requests" icon="prescription-bottle-alt">Pedidos</Navlink>
						<Navlink to="/laboratories" icon="vial">Laboratórios</Navlink>
						<Navlink to="/categories" icon="boxes">Categorias</Navlink>
						<Navlink to="/products" icon="box">Produtos</Navlink>
					</React.Fragment>
				}
			</div>
		</div>
	);
}