//Package
import * as React 			from 'react';

//Components
import { List, Body, Header as _Header, Column as _Column, Action as _Action }		from "./List";
// import { Search }           from '../components/input/text';
// import { Icon }             from "../components/button";

//Contexts
import systemcontext	from "../contexts/system";

export function Table ({children, src, paginate, update, ...props}) {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//states
	const [ perpage, setperpage]		= React.useState(5);
	const [ page, setpage ]				= React.useState(0);
	const [ pages, setpages ]			= React.useState(1);
	const [ data, setdata ]				= React.useState([]);
	const [ sort, setsort ]				= React.useState(null);
	const [ direction, setdirection ] 	= React.useState(false);
    const [ loading, setloading ]		= React.useState(true);
    const [ search, setsearch ]         = React.useState("");

	//contexts
	const { request }		= React.useContext(systemcontext);

	//-------------------------------------------------
	// Memos
	//-------------------------------------------------

	const sortfetch = React.useMemo(() => {
		return sort? ("&sort=" + sort + "," + (direction? "desc":"asc")):"";
    }, [sort, direction]);

    const searchfetch = React.useMemo(() => {
        if (search) return "&search=" + search;
        else        return "";
    }, [search]);

    const perpagefetch = React.useMemo(() => {
        if (perpage) return "&perpage=" + perpage;
        else        return "";
    }, [perpage]);

	//-------------------------------------------------
	// Callbacks
	//-------------------------------------------------

	//Get data from the server
	const fetch = React.useCallback(() => {
		request(src + "?page=" + (page + 1) + perpagefetch + searchfetch + sortfetch, {
			wait:false
		})
		.then(res => {
			setloading(false);
			setdata(res.data);
			setpages(res.last_page);
		});
	}, [request, page, perpagefetch, searchfetch, sortfetch, src]);

	//Update current page
	const pagination = React.useCallback(index => {
		setpage(index);
    }, []);

    //Update search
    const updateSearch = React.useCallback((data) => {
        setsearch(data);
    }, []);

	//-------------------------------------------------
	// Effects
	//-------------------------------------------------

	React.useEffect(() => {
		fetch();
	}, [page, search, perpage, sort, update, direction, fetch]);

	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	const body 		= [React.createElement(Body, {children:data,key:"body-data"})];
	const columns 	= React.Children.map(children, child => {

		if (child.type.name === "Header") {

			return React.cloneElement(child, {
				...child.props,
				children: React.Children.map(child.props.children, column => {

					if (column.type.name === "Column") {

						return React.cloneElement(column, {
							...column.props,
							direction: column.props.name === sort? direction:null,
							onClick: () => {
								const name = column.props.name;

								if (name === sort) {
									setdirection(!direction);
								}
								else {
									setsort(column.props.name);
									setdirection(false);
								}
							}
						});
					}
				})
			});
		}

		return child;
	});

	return (
        <React.Fragment>
            <div className="row mb-5">
                <div className="col">
                    {/* {
                        props.add &&

                        <Icon style={{display:"inline-block",height:45,width:45,margin:0,marginTop:-3}} onClick={props.add} icon="plus" className="btn" />
                    } */}
                    {/* {
                        props.search &&

                        <div style={{display:"inline-block"}} className="col-md-4 col-10">
                            <Search name="search" placeholder="Pesquisar..." onChange={updateSearch} />
                        </div>
                    } */}
                </div>
            </div>

            <List {...props} loading={loading} pagination={paginate && pagination} perpage={setperpage} page={page} total={pages}>
                {[...body,...React.Children.toArray(children), columns]}
            </List>
        </React.Fragment>
	);
}

//These are redefines of the list component, so we can import data from only one place

export const Header = _Header;
export const Action = _Action;
export const Column = _Column;
