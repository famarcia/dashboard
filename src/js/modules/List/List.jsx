//Core
import * as React 	from 'react';

//Context
const ListContext = React.createContext();

function List ({loading, children, pagination, sorteable, page = 0, total = 1, perpage = 25, ...props}) {

	//-------------------------------------------------
	// Properties
	//-------------------------------------------------

	//States
	const [ columns, 	setcolumns ] 	= React.useState([]);
	const [ rows,		setrows ]		= React.useState([]);
	const [ actions, 	setactions ]	= React.useState(null);

	//-------------------------------------------------
	// Effects
	//-------------------------------------------------

	React.useEffect(() => {
        //Extract components
        const childrenarray = React.Children.toArray(children);

        for (let i = 0; i < childrenarray.length; i++) {
            const child = childrenarray[i];

            if (child.type.listtype === "Header") {
                let localcolumns = [];

                React.Children.map(child.props.children, ({type, props}, i) => {

                    if (type.listtype === "Column") {
                        const { name, label, children, ...generic } = props;

                        localcolumns.push({
                            value: 		    name,
                            value_display:  label,
                            label: 		    children,
                            ...generic
                        });
                    }
                });

                setcolumns(localcolumns);
            }
            else if (child.type.listtype === "Body") {
                setrows(child);
            }
            else if (child.type.listtype === "Action") {
                setactions(child);
            }
        }
	}, [children]);


	//-------------------------------------------------
	// Functions
	//-------------------------------------------------

	const RenderColumns = () => {
        let response = [];

		for (let i = 0; i < columns.length; i++) {
			const column = columns[i];
			const direct = column.direction !== null && (sorteable !== null && sorteable) ? "direction " + (column.direction? "up":"down"):"";

			response.push(
				<th id={"column-" + column.value} key={"column-" + column.value} className={column.className}>
					<button onClick={column.onClick} className={direct}>
						{column.label}
					</button>
				</th>
			);
		}

		//Add actions if required
        if (actions) response.push(<th key="actioncontrol" />);

		return response;
	}

	const RenderRows = () => {
        let response = [];

		for (let i = 0; i < rows.props.children.length; i++) {
			let row = [];
            const rowcolumn = rows.props.children[i];


			for ( let colindex = 0; colindex < columns.length; colindex++) {
                const column 	= columns[colindex];
				const show 		= column.modifier? column.modifier(rowcolumn[column.value], rowcolumn):rowcolumn[column.value];
				const limit 	= (typeof show === "string" && column.limit && show.length > column.limit)? show.substring(0, column.limit):show;

				row.push(
					<td key={rowcolumn.id + "-" + column.value} className={column.childClass}>{limit}</td>
				);
			}

			//Add actions if required
			if (actions) {
				let clonedAction = React.cloneElement(actions, {...actions.props, row:rowcolumn});
				row.push(<td key={rowcolumn.id + "-action"}>{clonedAction}</td>);
			}

			response.push(
				<tr key={rowcolumn.id}>{row}</tr>
			);
		}

		return response;
	}

	const RenderPagination = () => {
		//No pagination required
		if (!pagination) return null;

		let response = [];

		//Count pages
		for (let i = 0; i < total; i++) {
			response.push(
				<div key={i} className={"pagination-item " + (i === page? "active":"")} onClick={() => pagination(i)}><span>{i + 1}</span></div>
			);
		}

		return response;
	}

	const RenderPerPage = () => {
		//No perpage change required
		if (!perpage) return null;

		return "";
	}

	const RenderList = () => {
		//Data has not been loaded yet
		if (loading) {
			return (
				<h4 className="text-center mt-5 mb-5">Carregando</h4>
			);
		}

		//There are no entries
		if (rows.props.children.length === 0) {
			return (
				<h4 className="text-center mt-5 mb-5">Não existem valores</h4>
			);
        }

		//There are entries
		return (
			<React.Fragment>
				<table {...props}>
					<thead>
						<tr>
                            {RenderColumns() }
						</tr>
					</thead>

					<tbody>
						{RenderRows ()}
					</tbody>
				</table>

				<div className="pagination">
					{RenderPagination ()}
				</div>
				<div className="perpage">
					{RenderPerPage ()}
				</div>
			</React.Fragment>
		);
	}

	//-------------------------------------------------
	// Render
    //-------------------------------------------------

	return (
		<ListContext.Provider value={{setcolumns, setrows}}>
			{RenderList()}
		</ListContext.Provider>
	);
}

//Bind it to the dist react, so we can keep track of what this is
List.listtype = "List";

export default List;
