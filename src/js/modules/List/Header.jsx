function Header ({children}) {
	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return children;
}

//Bind it to the dist react, so we can keep track of what this is
Header.listtype = "Header";

export default Header;
