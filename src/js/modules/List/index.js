//Components
import _Action   from "./Action";
import _Body     from "./Body";
import _Column   from "./Column";
import _Header   from "./Header";
import _List     from "./List";
import _Row      from "./Row";

//Separated components
export const Action     = _Action;
export const Body      = _Body;
export const Column    = _Column;
export const Header    = _Header;
export const List      = _List;
export const Row       = _Row;

//Bundle all components
const bundle = {
    Action:    _Action,
    Body:      _Body,
    Column:    _Column,
    Header:    _Header,
    List:      _List,
    Row:       _Row,
};

export default bundle;
