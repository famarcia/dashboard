function Column ({children}) {
	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return children;
}

//Bind it to the dist react, so we can keep track of what this is
Column.listtype = "Column";

export default Column;
