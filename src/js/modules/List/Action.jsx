//Core
import * as React 	from 'react';

function Action (props) {
	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return React.Children.map(props.children, child => {
		let { onClick, ...childprops } = child.props;

		return React.cloneElement(child, {
			childprops,
			onClick: () => {
				onClick(props.row);
			}
		});
	});
}

//Bind it to the dist react, so we can keep track of what this is
Action.listtype = "Action";

export default Action;
