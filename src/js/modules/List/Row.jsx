function Row ({children}) {
	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return children;
}

//Bind it to the dist react, so we can keep track of what this is
Row.listtype = "Row";

export default Row;
