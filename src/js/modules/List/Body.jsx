function Body ({children}) {
	//-------------------------------------------------
	// Render
	//-------------------------------------------------

	return children;
}

//Bind it to the dist react, so we can keep track of what this is
Body.listtype = "Body";

export default Body;
