//Interfaces
import { ReactChildren, ReactChild, ReactElement } from "react";

export interface iReactProps {
    //Default
    children? : Array<ReactElement> | ReactChildren | ReactChild | Object

    //Extra
    value? : any,
    [index : string] : any,
}

export interface iReactChildrenArray {
    type? : string
}