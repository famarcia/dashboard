//Interfaces
import { AxiosInstance } from "axios";

declare global {
    interface Window {
        url 		    (value:string, endpoint?: string|null)                  : string,
        asset		    (value:string)                                          : string,
        img			    (value:string)                                          : string,
        file		    (value:string)                                          : string,
        lerp            (from : number, to : number, smooth : number)           : number,
        smoothScroll    (elementTarget : string, elementScrolling? : string)    : void,
        csrf                                                                    : string,
        Pusher                                                                  : Object,
		masker                                                                  : Object,
		axios																	: AxiosInstance
    }
}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios 												= require('axios');
window.axios.defaults.withCredentials 						= true;
window.axios.defaults.baseURL 								= process.env.REACT_APP_URL_API;