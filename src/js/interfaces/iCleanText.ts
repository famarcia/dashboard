export interface iCleanBase {
    blocks  : Array<iCleanBlock>,
    version : string,
    time    : number
}

export interface iCleanBlock {
    type : string,
    data : {
        text? : string
    }
}