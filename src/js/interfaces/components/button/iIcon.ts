export default interface iIcon {
    icon : string,

    //Other props
    [x : string] : any
}