import { ReactElement } from "react";

export default interface iDefaultProps {
    children        : ReactElement | string | any,
    className?      : string,
    background?     : string,
    group?          : string,
    icon?           : boolean | number,
    size?           : number,
    color?          : string,
    href?           : string | null,
    to?             : string | null,
    onClick?    ()  : void,

    //Generic
    [index : string] : any
}