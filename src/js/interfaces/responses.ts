//Interfaces
import iUser from "./user";

export interface iLoginResponse {
	data 	: iUser,
	token	: string,
}