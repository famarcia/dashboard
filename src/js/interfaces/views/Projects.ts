export interface iProject {
    id          : string,
    label       : string,
    image       : string | null,
    link        : string | null,
    description : Object | null,
    tags        : Array<iTag>,
}

export interface iTag {
    id          : string,
    label       : string,
}