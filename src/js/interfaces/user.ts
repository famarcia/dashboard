export default interface iUser {
	id? 			: string,
	name 			: string,
	email			: string,
	description?	: string,
}