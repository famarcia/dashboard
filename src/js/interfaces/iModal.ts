export default interface iModal {
    name        : string,
    fullscreen  : boolean,
    className   : string,
    large       : boolean,
    closeable   : boolean,
    options     : Object,
}

export interface iModalContext {
    addModal    (value : iModal)    : void,
    removeModal (value : string)    : void,
    show        (value : string)    : void,
    hide        ()                  : void,
    display                         : string,
    fullscreen                      : boolean,
    expand      ()                  : void,
    exists      (value : string)    : boolean,
}

export interface iModalList {
    [key : string] : iModal
}